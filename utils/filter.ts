export const removeEmptyParams = (params: object) => {
  const filteredParams: { [key: string]: string } = {};
  Object.entries(params).forEach(([key, value]) => {
    if (value) {
      filteredParams[key] = value.toString();
    }
  });

  return filteredParams;
};
