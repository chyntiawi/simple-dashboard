import dayjs from "dayjs";

export const format = (
  dateString: string,
  template: string = "DD-MM-YYYY HH:mm"
) => dayjs(dateString).format(template);
