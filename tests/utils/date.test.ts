import { format } from "../../utils/date";

describe("Date Utils", () => {
  describe("no template params", () => {
    const result = format("2021-01-01T00:00Z");

    it("should format to DD-MM-YYYY HH:mm", () => {
      expect(result).toBe("01-01-2021 07:00");
    });
  });

  describe("has template params", () => {
    const result = format("2021-01-01T00:00Z", "DD MMM YYYY, HH:mm");

    it("should format to DD MMM YYYY, HH:mm", () => {
      expect(result).toBe("01 Jan 2021, 07:00");
    });
  });
});
