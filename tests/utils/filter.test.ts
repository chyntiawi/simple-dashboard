import { removeEmptyParams } from "../../utils/filter";

describe("Filter Utils", () => {
  describe("remove empty params", () => {
    const result = removeEmptyParams({ page: 1, sortBy: "" });

    it("should return new params correctly", () => {
      expect(result).toEqual({ page: "1" });
    });
  });
});
