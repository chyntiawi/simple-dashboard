import "@testing-library/jest-dom";
import { rest } from "msw";
import { setupServer } from "msw/node";
import { fireEvent, render, screen } from "@testing-library/react";
import Home from "../../pages/index";

describe("Simple Dashboard", () => {
  const apiUrl = "https://randomuser.me/api";
  const server = setupServer(
    rest.get(apiUrl, (_: any, res: any, context: any) => {
      return res(
        context.json({
          results: [
            {
              login: {
                username: "silverbear",
                uuid: "9bed6f-a039-52329fc8",
              },
              name: {
                first: "Manuel",
                last: "Morel",
              },
              email: "manuel.morel@example.com",
              gender: "male",
              registered: {
                date: "2002-10-10T04:41:52.582Z",
              },
            },
          ],
        })
      );
    })
  );

  beforeAll(() => server.listen());
  afterEach(() => server.resetHandlers());
  afterAll(() => server.close());

  describe("when fetch users is success", () => {
    const testCases = [
      ["username", "silverbear"],
      ["name", "Manuel Morel"],
      ["email", "manuel.morel@example.com"],
      ["gender", "male"],
      ["registered date", "10-10-2002 11:41"],
    ];

    it.each(testCases)("renders %s correctly", async (_, expected) => {
      render(<Home />);

      expect(await screen.findByText(expected)).toBeInTheDocument();
    });
  });

  describe("when fetch users is failed", () => {
    it("renders an error message", async () => {
      server.use(
        rest.get(apiUrl, (_: any, res: any, context: any) => {
          return res(context.status(500));
        })
      );

      render(<Home />);

      expect(
        await screen.findByText("An error occurred, please try again later.")
      ).toBeInTheDocument();
    });
  });

  describe("when reset filter", () => {
    it("changes filter to default value", async () => {
      render(<Home />);

      const genderInput = (await screen.findByTestId("select-gender"))
        .firstElementChild!;
      fireEvent.mouseDown(genderInput);

      const femaleOption = await screen.findByText("Female");
      fireEvent.click(femaleOption);

      expect(genderInput).toHaveTextContent("Female");

      const resetFilterBtn = await screen.findByTestId("reset-filter");
      fireEvent.click(resetFilterBtn);

      expect(genderInput).toHaveTextContent("All");
    });
  });
});
