import "whatwg-fetch";

window.matchMedia =
  window.matchMedia ||
  function () {
    return {
      matches: false,
      addListener: jest.fn(),
      removeListener: jest.fn(),
    };
  };

jest.mock("next/config", () => () => ({
  publicRuntimeConfig: {
    apiUrl: "https://randomuser.me/api",
  },
}));
