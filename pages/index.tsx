import { Button, Card, Col, Input, message, Row, Select, Table } from "antd";
import type { NextPage } from "next";
import getConfig from "next/config";
import Head from "next/head";
import { useEffect, useState } from "react";
import homeStyles from "../styles/home.module.css";
import { format as formatDate } from "../utils/date";
import { removeEmptyParams } from "../utils/filter";

const Home: NextPage = () => {
  const [users, setUsers] = useState([]);

  const [pagination, setPagination] = useState({
    page: 1,
    sortBy: "",
    sortOrder: "",
  });

  const [keyword, setKeyword] = useState("");

  const filterDefault = { gender: "" };
  const [filter, setFilter] = useState(filterDefault);

  const [loading, setLoading] = useState(false);

  const { page, sortBy, sortOrder } = pagination;

  async function fetchUsers() {
    const { publicRuntimeConfig } = getConfig();
    const { apiUrl } = publicRuntimeConfig;

    try {
      setLoading(true);

      const filteredParams = removeEmptyParams({
        ...pagination,
        ...filter,
        keyword,
      });
      const params = new URLSearchParams({
        ...(filteredParams as any),
        pageSize: "10",
        results: "20",
      });
      const res = await fetch(`${apiUrl}?${params}`);
      const { results } = await res.json();

      setUsers(results);
    } catch {
      message.error("An error occurred, please try again later.");
    } finally {
      setLoading(false);
    }
  }

  const { gender } = filter;

  useEffect(() => {
    fetchUsers();
  }, [page, sortBy, sortOrder, keyword, gender]);

  const columns = [
    {
      title: "Username",
      dataIndex: ["login", "username"],
      key: "username",
      sorter: true,
    },
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
      sorter: true,
      render: (_: any, record: any) =>
        `${record.name.first} ${record.name.last}`,
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
      sorter: true,
    },
    {
      title: "Gender",
      dataIndex: "gender",
      key: "gender",
      sorter: true,
    },
    {
      title: "Registered Date",
      dataIndex: ["registered", "date"],
      key: "registeredDate",
      sorter: true,
      render: (date: string) => formatDate(date),
    },
  ];

  const onSearch = (value: string) => setKeyword(value);

  const onFilterChange = (filterBy: string) => (value: string) =>
    setFilter({ ...filter, [filterBy]: value });

  const resetFilter = () => setFilter(filterDefault);

  const onTableChange = (pagination: any, _: any, sorter: any) => {
    let sortBy = "";
    let sortOrder = "";
    if (sorter.order) {
      sortBy = sorter.columnKey;
      sortOrder = sorter.order;
    }

    setPagination({
      page: pagination.current,
      sortBy,
      sortOrder,
    });
  };

  const { Search } = Input;

  const { Option } = Select;

  return (
    <>
      <Head>
        <title>Simple Dashboard</title>
        <meta name="description" content="Simple Dashboard" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Card>
        <h1>Simple Dashboard</h1>

        <Row gutter={[0, 36]}>
          <Col span={24}>
            <Row gutter={12}>
              <Col span={6}>
                <Search
                  placeholder="Search..."
                  onSearch={onSearch}
                  enterButton
                />
              </Col>
              <Col span={6}>
                <Select
                  data-testid="select-gender"
                  className={homeStyles["users-filter"]}
                  value={filter.gender}
                  onChange={onFilterChange("gender")}
                >
                  <Option value="">All</Option>
                  <Option value="male">Male</Option>
                  <Option value="female">Female</Option>
                </Select>
              </Col>
              <Col>
                <Button data-testid="reset-filter" onClick={resetFilter}>
                  Reset Filter
                </Button>
              </Col>
            </Row>
          </Col>
          <Col span={24}>
            <Table
              columns={columns}
              rowKey={(record) => record.login.uuid}
              dataSource={users}
              loading={loading}
              onChange={onTableChange}
            />
          </Col>
        </Row>
      </Card>
    </>
  );
};

export default Home;
