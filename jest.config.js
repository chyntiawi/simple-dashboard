module.exports = {
  moduleNameMapper: {
    "^.+\\.module\\.(css)$": "identity-obj-proxy",
  },
  transform: {
    "^.+\\.(js|ts|tsx)$": ["babel-jest", { presets: ["next/babel"] }],
  },
  transformIgnorePatterns: ["/node_modules/", "^.+\\.module\\.(css)$"],
  collectCoverageFrom: ["<rootDir>/pages/**/*.tsx", "<rootDir>/utils/**/*.ts"],
  testPathIgnorePatterns: ["<rootDir>/node_modules/", "<rootDir>/.next/"],
  testEnvironment: "jsdom",
  setupFiles: ["<rootDir>/tests/setup/index.js"],
};
